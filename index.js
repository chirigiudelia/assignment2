
const FIRST_NAME = "Delia";
const LAST_NAME = "Chirigiu";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache = {};
   var counter = {};
   cache.pageAccessCounter = function(section='home'){
    section = section.toLowerCase();
    if (counter[section] === undefined)
        counter[section] = 1;  
    else
        counter[section]++;
   };
   cache.getCache = function(){
    return counter;
   };
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

